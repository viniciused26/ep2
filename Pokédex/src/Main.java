public class Main{
	public static void main(String[] args) {
		Pokedex pokedex = new Pokedex();
		pokedex.readFirstCSVFile();
		pokedex.readSecondCSVFile();
		pokedex.printPokemonData();
	}
}