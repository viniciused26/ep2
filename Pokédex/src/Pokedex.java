import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Pokedex{
	String[] firstListData = new String[8665];
	String[] secondListData = new String[10109];
	
	public void readFirstCSVFile(){
		int dataID = 0;
		
		String fileName = "../data/csv_files/POKEMONS_DATA_1.csv";
		File csvFile = new File(fileName);
		
		try {
			Scanner inputStream = new Scanner(csvFile);
			inputStream.useDelimiter(",");
			while(inputStream.hasNext()) {
				String data = inputStream.next();
				firstListData[dataID] = data;
				dataID++;
			}
			
			inputStream.close();
				
				
				
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public void readSecondCSVFile(){
		int dataID = 0;
		
		String fileName = "../data/csv_files/POKEMONS_DATA_2.csv";
		File csvFile = new File(fileName);
		
		try {
			Scanner inputStream = new Scanner(csvFile);
			inputStream.useDelimiter(",");
			while(inputStream.hasNext()) {
				String data = inputStream.next();
				secondListData[dataID] = data;
				dataID++;
			}
			
			inputStream.close();
				
				
				
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public void printPokemonData() {
	}
	
	
}